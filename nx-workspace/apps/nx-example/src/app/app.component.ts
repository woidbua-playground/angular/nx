import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { first } from 'rxjs';
import { Todo } from '@nx-workspace/data';

@Component({
  selector: 'nx-workspace-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  todos: Todo[] = [];

  constructor(private http: HttpClient) {
    this.fetch();
  }

  addTodo() {
    this.http.post('/api/addTodo', {}).pipe(first()).subscribe(() => {
      this.fetch();
    });
  }

  private fetch() {
    this.http.get<Todo[]>('/api/todos').pipe(first()).subscribe(t => this.todos = t);
  }
}
